---
title: Danger Swift
subtitle: "Danger in Swift"
layout: guide_js
edit_url: https://gitlab.com/danger-systems/danger.systems/blob/master/static/source/js/swift.html.md
---

With [Danger JS 2.0](https://spectrum.chat/danger/announcements?thread=8a9c08c5-bafe-4272-b41e-f6bb34fc1b45) the 
ability to allow other processes to handle Dangerfiles introduced the theory of letting other ecosystems have their
cake and eat it too. The first production-ready ecosystem is Swift.

Danger Swift lets you run a `Dangerfile.swift` which compiles and evaluates your rules using the DSL from Danger. The 
entire DSL you use is built in Swift - so it's a first class citizen. For example, here's a trivial example that checks
for CHANGELOG entries on every PR when you edit files in a `Sources/` directory:

```swift
import Danger

let allSourceFiles = danger.git.modifiedFiles + danger.git.createdFiles

let changelogChanged = allSourceFiles.contains("CHANGELOG.md")
let sourceChanges = allSourceFiles.first(where: { $0.hasPrefix("Sources") })

if !changelogChanged && sourceChanges != nil {
  warn("No CHANGELOG entry added.")
}
```

Looks and feels like a Swift script, because it is.

### How do I use it?

You'll need to do the same [setup for authentication tokens](http://danger.systems/js/guides/getting_started.html#creating-a-bot-account-for-danger-to-use)
as Danger JS in your CI, but you differ on the CI setup slightly.

```yaml
// install both danger-js and danger-swift
- npm install -g danger
- brew install danger/tap/danger-swift

// run
- danger process danger-swift
```

### How stable is it?

Danger Swift has been running in production for 3 months, and is considered good to go. If you're looking for some references:

- [Moya/Harvey](https://github.com/Moya/Harvey) - [PR introducing Danger Swift](https://github.com/Moya/Harvey/pull/13)

### I want to get started

Check out the [README inside Danger Swift](https://github.com/danger/danger-swift#danger-swift) for more detailed instructions.

### How does it compare to the Ruby or the JS version?

Both Ruby and JS versions of danger have been running for years, so they're pretty mature now. Danger Swift only
contains the attributes for your PR metadata, so it's enough for the majority of rules.

There is no plugin architecture, so plugins you may be using for Ruby and JS won't work: you'll need to re-create them.

Unlike Ruby and JS, Danger Swift is considered a community project. This means that the onus to improve it lays
with the people who use it, rather then Danger Ruby and JS which are fully supported by Orta. We have a space on the
[danger spectrum](https://spectrum.chat/danger) for discussion around [Danger Swift](https://spectrum.chat/danger/swift).

I wouldn't take this as a reason to avoid it. TBH, I'd take it as [a project to own and improve](https://github.com/danger/danger-swift#what-are-the-next-big-steps). 
You don't want it being ran by someone with no interest in the ecosystem anyway.

