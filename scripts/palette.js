const Vibrant = require("node-vibrant")
const fs = require("fs")
const Jimp = require("jimp")
const child_process = require("child_process")

for (let index = 0; index < 3; index++) {
  const conwayImagePath = `static/source/images/js/cloudyconway_original_${index}.png`
  const cosmosImagePath = `static/source/images/js/crookedcosmos_original_${index}.png`

  let v = new Vibrant(conwayImagePath)

  const swatchJSONMap = swatch => ({
    hex: swatch.getHex(),
    hsl: swatch.getHsl()
  })
  
  const modifySwatchByLuminenceIfMax = (swatch, lumDiff, max) => {
    const l = swatch.hsl[2]
    if (l > lumDiff) {
      const rgb = Vibrant.Util.hslToRgb(swatch.hsl[0], swatch.hsl[1], swatch.hsl[2] - lumDiff)
      swatch.hex = Vibrant.Util.rgbToHex(rgb[0], rgb[1], rgb[2])
    }
    return swatch
  }
  
  v.getPalette((err, palette) => {
    console.log(palette)
    // Need a lot of fallbacks :D
    try {
      const swatches = {
        vibrant: swatchJSONMap(palette.Vibrant || palette.LightVibrant || palette.DarkVibrant),
        "vibrant-light": swatchJSONMap(palette.LightVibrant || palette.Vibrant || palette.DarkVibrant),
        "vibrant-dark": swatchJSONMap(palette.DarkVibrant || palette.Vibrant || palette.LightVibrant),
        muted: swatchJSONMap(palette.Muted || palette.LightMuted || palette.DarkMuted),
        "muted-light": swatchJSONMap(palette.LightMuted || palette.Muted || palette.DarkMuted),
        "muted-dark": swatchJSONMap(palette.DarkMuted || palette.Muted || palette.LightMuted)
      }
      
        // Ensure a max lightness: https://gitlab.com/danger-systems/danger.systems/issues/112
        swatches["muted"] = modifySwatchByLuminenceIfMax(swatches["muted"], 0.2, 0.8)
        swatches["muted-light"] = modifySwatchByLuminenceIfMax(swatches["muted-light"], 0.05, 0.8)
        swatches["muted-dark"] = modifySwatchByLuminenceIfMax(swatches["muted-dark"], 0.25, 0.8)
      
        writeJSON(swatches)
        writeSCSS(swatches)
        updateSVGs(swatches)
        writeDangerLogos()
        writePerilLogos()
        writeToCanonicalJSONRep()
      
    } catch (error) {
      return
    }
  })
  
const writeToCanonicalJSONRep = () => {
  fs.writeFileSync(`static/source/images/js/cloudyconway_tweet_oembed.json`, fs.readFileSync(`static/source/images/js/cloudyconway_tweet_oembed_${index}.json`, "utf8"))
}

  const writeJSON = swatches => {
    const jsonPath = "static/json_data/js_color_scheme.js"
    fs.writeFileSync(jsonPath, JSON.stringify(swatches))
  }
  
  const writeSCSS = swatches => {
    const scssPath = "static/source/stylesheets/colors-js.scss"
    const scss = Object.keys(swatches).map(key => `$${key}: ${swatches[key].hex};`)
    fs.writeFileSync(scssPath, scss.join("\n"))
  }
  
  const updateSVGs = swatches => {
    const originalCSSHex = "#F0D34B"
    const before = "static/source/images/js/_before.svg"
    const beforeRender = "static/source/images/js/before.svg"
    const after = "static/source/images/js/_after.svg"
    const afterRender = "static/source/images/js/after.svg"
  
    const updateFile = (file, newFile, oldColor, newColor) => {
      const original = fs.readFileSync(file, "utf8")
      const modified = original.replace(RegExp(oldColor, "g"), newColor)
      fs.writeFileSync(newFile, modified)
    }
    updateFile(before, beforeRender, originalCSSHex, swatches["muted-light"].hex)
    updateFile(after, afterRender, originalCSSHex, swatches["muted"].hex)
  }
  
  const writeDangerLogos = () => {
    const largeMaskPath = "static/source/images/js/masks/danger-logo-mask-hero@2x.png"
    const heroImagePath = "static/source/images/js/danger-logo-hero@2x.png"
    const heroUncachedImagePath = "static/source/images/js/danger-logo-hero-cachable@2x.png"
    const smallLogoPath = "static/source/images/js/danger-logo-small@2x.png"
  
    Promise.all([Jimp.read(conwayImagePath), Jimp.read(largeMaskPath)])
      .then(([bg, mask]) => {
        bg
          .cover(mask.bitmap.width, mask.bitmap.height)
          .mask(mask, 0, 0)
          .crop(0, 0, mask.bitmap.width, mask.bitmap.height)
          .write(heroImagePath)
          .write(heroUncachedImagePath)
          .resize(252, 80)
          .write(smallLogoPath)
      })
      .catch(function(err) {
        console.error(err)
      })
  
    const renderMask = (mask, to) => {
      return Promise.all([Jimp.read(conwayImagePath), Jimp.read(mask)])
        .then(([bg, mask]) => {
          bg
            .cover(mask.bitmap.width, mask.bitmap.height)
            .mask(mask, 0, 0)
            .crop(0, 0, mask.bitmap.width, mask.bitmap.height)
            .write(to)
        })
        .catch(function(err) {
          console.error(err)
        })
    }
    const jsRenderedPath = "static/source/images/home/js-logo@2x.png"
    const jsMaskPath = "static/source/images/js/masks/js-logo-mask@2x.png"
    renderMask(jsMaskPath, jsRenderedPath)
  
    const swiftMaskPath = "static/source/images/js/masks/swift-logo-mask@2x.png"
    const swiftRenderedPath = "static/source/images/home/swift-logo@2x.png"
    renderMask(swiftMaskPath, swiftRenderedPath)
  }
  
  
  const writePerilLogos = swatches => {
    const largeMaskPath = "static/source/images/js/masks/peril-logo-mask-hero@2x.png"
    const heroImagePath = "static/source/images/js/peril-logo-hero@2x.png"
    const heroUncachedImagePath = "static/source/images/js/peril-logo-hero-cachable@2x.png"
    const smallLogoPath = "static/source/images/js/peril-logo-small@2x.png"
  
    Promise.all([Jimp.read(cosmosImagePath), Jimp.read(largeMaskPath)])
      .then(([bg, mask]) => {
        bg
          .cover(mask.bitmap.width, mask.bitmap.height)
          .mask(mask, 0, 0)
          .crop(0, 0, mask.bitmap.width, mask.bitmap.height)
          .write(heroImagePath)
          .write(heroUncachedImagePath)
          .resize(252, 80)
          .write(smallLogoPath)
      })
      .catch(function(err) {
        console.error(err)
      })
  }
    
}
